{include file="head.tpl"}

	<div id="content">
		<h1 lang="en">ROCKS FALL - EVERYONE DIES</h1>
		<p lang="en">
			Alexandria is being updated. Please try again in a couple of minutes.
		</p>
		<p lang="da">
			Alexandria er under opdatering. Prøv igen om et par minutter.
		</p>
		<p lang="sv">
			Alexandria uppdateras. Försök igen om ett par minuter.
		</p>
		<p lang="nb">
			Alexandria oppdateres. Prøv igjen om et par minutter.
		</p>
		<p lang="de">
			Alexandria wird derzeit aufdatiert. Versuche in ein Paar Minuten nochmals.
		</p>
	</div>

{include file="end.tpl"}
